# AITM 2019

Here there is a working environment to start playing and learning DevOps.

### Tools you need to run the code

If you want to use the code from this repo, you will need the following tools:

* [git](https://git-scm.com)  
* [Vagrant](https://www.vagrantup.com)  
* [Virtualbox](https://www.virtualbox.org)  
* [Visual Studio Code](https://code.visualstudio.com)  (Optional)

### Installation for Local Development

This setup deploys a Ubuntu 18.04 VM (from now on the Box) using the following command:

```
    vagrant up
```

### Installation for Digital Ocean

To run the set up on Digital Ocean, we need a plugin for that:

```
    vagrant plugin install vagrant-digitalocean
```

Additionally, we have to create a SSH Keygen:

```
    ssh-keygen -t rsa
```

This setup deploys a Ubuntu 18.04 VM (from now on the Box) using the following command:

```
    vagrant up --provider=digital_ocean
```


The box contains docker engine, docker compose and docker machine. It is the minimum working environment to start using Kubernetes.

## Note

If you find that the folder src is empty, type this command (It is possible that a Bitbucket user is needed):

```
    git submodule update --init --recursive
```

## Potential problems in Windows 10

There are some known issues in Windows 10 when the users are starting the Vagrant environment.

Some changes must be changed in the Vagrantfile

```
    for i in 30000..30100
      devops.vm.network :forwarded_port, guest: i, host: i
    end
```

## Installing Jenkins

Jenkins is easily installable using Docker. Nonetheless, Vagrantfile provisions your instance.

To install Jenkins use this command:

```
    docker run -d --name jenkins -p 8080:8080 -p 50000:50000 jenkins/jenkins:lts
```

## Installing Sonarqube

Sonarqube is easily installable using Docker.

To install Jenkins use this command:

```
    docker run -d --name sonarqube -p 9000:9000 -d sonarqube
```

## Sonarqube properties for the-example-app

If you want to analyze the code, into Sonarqube, you need to add this to steps in Jenkins. 
Jenkins needs Sonarqube Scanner, Java and a running Sonarqube server to run this part.
The configuration is as follows:

```
sonar.projectKey=sonar-js
sonar.projectName=sonar-js
sonar.projectVersion=${BUILD_NUMBER}
sonar.sources=public,routes,services,views
sonar.tests=test
sonar.language=js
```


### Support

This tutorial is released into the public domain by [ITNove](http://www.itnove.com) under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hern�ndez Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact ITNove for further details.

* ITNove
* Carrer Valencia 63, Agora CN
* 08015 Barcelona
* T: +34 679 97 71 87

[![ITNOVE](https://www.itnove.com/sites/all/themes/blocks-drupal/logo.png)](http://www.itnove.com/)
